package com.example.restapi

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_dialog.*
import kotlinx.android.synthetic.main.fragment_dialog.view.*

class DialogFragment: android.support.v4.app.DialogFragment() {

    lateinit var activityInterractor : ActivityInterractor

    @SuppressLint("NewApi")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rootView: View = inflater.inflate(R.layout.fragment_dialog, container, false)

        rootView.btn_change_date_and_time.setOnClickListener() {
            TittleAndMessageClass.Tittle = editTextDate.text.toString()
            TittleAndMessageClass.Message = editTextTime.text.toString()

            Toast.makeText(context, "Дата и время успешно изменены", Toast.LENGTH_LONG).show()

            dismiss()
        }

        return rootView
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ActivityInterractor) {
            this.activityInterractor = context
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activityInterractor.OnFragmentClosed()
    }
}
